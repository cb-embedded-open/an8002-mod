# AN8002 Log Mod Project : Failed

EDIT2: someone on the internet already exposed the Tx pin from the encapsulant, see on eevblog : [Link on eevblog](https://www.eevblog.com/forum/testgear/an8008-us-$19-10000count-1uv-0-01ua-0-01ohm-resolution-meter/300/)

![Under the encapsulant](Under%20the%20encapsulant.jpeg)

**EDIT: Not possible, TX pin under epoxy blob. Only I2C Memory dump / mod worked.**

The initial idea was to modify the AN8002 Multimeter to enable logging of the data on a computer through the serial port.

The AN8002 Multimeter has a **DTM0660 IC**. It communicates with a **24C02A** EEPROM that stores calibration data and settings.

I have dumped the I2C memory, and did the proposed modification :
* extend backlight timing to 30s
* Long press REL to enable Tx.
* (See kerry wong tutorial)

![Wiring](doc/DTM0660_wiring.jpg)

I initially thought that the Tx pin was accessible, but finally, it seams not, the Rx "TST" pin is usually accessible as it is used for calibration.
The Tx pin is usually not accessible (DTM0660 schematic in datasheet). In my case, it seems to be under the epoxy blob.

![Schematic from datasheet (eevblog)](doc/schematic.png)

# Button Wiring does not match the schematic

I do not understand the button wiring. Indeed, on the schematic it is kind of matricial, but on my multimeter, it does not always match : for instance, the internal **RANGE** button is enabled by shorting **PT1.1**, but on the schematic, it is enabled by shorting **PT1.0**.

![Mismatch](doc/mismatch.jpg)

# Documentations and tutorials

Translated datasheets :
* [DTM0660 Eevblog (complete)](https://www.eevblog.com/forum/testgear/an8008-us-$19-10000count-1uv-0-01ua-0-01ohm-resolution-meter/?action=dlattach;attach=333782)
* [DTM0660 Kerrywong (partial)](http://www.kerrywong.com/blog/wp-content/uploads/2016/04/DTM0660DataSheet.pdf)

Analysis, teardowns and tutorials :
* [Full teardown / analysys](http://lygte-info.dk/review/DMMAnengAN8002%20UK.html)
* [Another Teardown](https://www.markhennessy.co.uk/budget_multimeters/aneng_an8002.htm)
* [Kerry Wong Hack tutorial](http://www.kerrywong.com/2016/03/19/hacking-dtm0660l-based-multimeters/)
