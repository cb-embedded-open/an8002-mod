
# I2C 24C02A Memory

Data sniffed with DSView Logic Analyzer : [CSV File](Sniffed/eeprom_data_binary.csv)
Data dumped with CH341A : [Dump](Dumps/AN8002.bin)

Dump with ch341eeprom : https://github.com/command-tab/ch341eeprom

```bash
./ch341eeprom -v -s 24c02 -r AN8002.bin
```

## Meas

Array of "meas" values for dial functionality configuration.

| Hex | 0x80-0x8F | 0x90-0x9F | 0xA0-0xAF | 0xB0-0xBF |
| --- | --- | --- | --- | --- |
| 0 | 00 | 00 | 00 | 00 |
| 1 | 00 | 00 | 00 | 00 |
| 2 | 00 | 00 | 00 | 00 |
| 3 | 00 | 00 | 00 | 00 |
| 4 | 00 | 00 | 00 | 00 |
| 5 | 00 | 00 | 00 | 00 |
| 6 | 10 | 11 | 00 | 00 |
| 7 | 13 | 15 | 00 | 00 |
| 8 | 00 | 00 | 00 | 00 |
| 9 | 0E | 0F | 00 | 00 |
| A | 00 | 00 | 00 | 00 |
| B | 07 | 09 | 0A | 0B |
| C | 00 | 00 | 00 | 00 |
| D | 12 | 00 | 00 | 00 |
| E | 03 | 04 | 00 | 00 |
| F | 01 | 02 | 00 | 00 |